//
//  ViewController.swift
//  Todo
//
//  Created by Lazar Jovicic on 05/09/2020.
//  Copyright © 2020 Lazar Jovicic. All rights reserved.
//

import UIKit
import CoreData

class TodoViewController: UITableViewController {
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var itemArray = [Item]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cells.ToDoItemCell, for: indexPath)
        cell.textLabel?.text = itemArray[indexPath.row].title
        cell.textLabel?.textColor = UIColor.black
        if itemArray[indexPath.row].done {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        
        return cell
    } 
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        context.delete(itemArray[indexPath.row])
        itemArray.remove(at: indexPath.row)
        saveData()
        tableView.deselectRow(at: indexPath, animated: true)
            tableView.reloadData()
    }
    @IBAction func addNewItem(_ sender: UIBarButtonItem) {
        var textField = UITextField()
        
        let alert = UIAlertController(title: "Add new TODO item", message: "", preferredStyle: .alert)
        let action = UIAlertAction(title: "Add Item", style: .default) { (action) in
            
            if let newText = textField.text, textField.text != "" {
                let newItem = Item(context: self.context )
                newItem.done = false
                newItem.title = newText
                self.saveData()
                self.itemArray.append(newItem)
                self.tableView.reloadData()
            }
        }
        alert.addAction(action)
        alert.addTextField { (alertTextField) in
            alertTextField.placeholder = "create new item"
            textField = alertTextField
        }
        present(alert, animated: true, completion: nil)
    }
    
    func  saveData() {
        do {
            try self.context.save()
            
        } catch {
            print("Unable to save data \(error)")
        }
    }
    
    func loadData() {
        let request : NSFetchRequest<Item> = Item.fetchRequest()
        do {
           itemArray = try context.fetch(request)
        } catch {
            print("error getting data from db \(error)")
        }
    }
}
