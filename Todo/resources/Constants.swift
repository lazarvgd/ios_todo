//
//  Constants.swift
//  Todo
//
//  Created by Lazar Jovicic on 05/09/2020.
//  Copyright © 2020 Lazar Jovicic. All rights reserved.
//

import Foundation

class Constants {
    
    struct Cells {
        static let ToDoItemCell = "ToDoItemCell"
    }
    
}
